import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-app-signup',
	templateUrl: './app-signup.page.html',
	styleUrls: ['./app-signup.page.scss'],
})
export class AppSignupPage implements OnInit {

	public name: string = '';
	public email: string = '';
	public password: string = '';
	public password_confirm: string = '';
	public avatar: string = '';
	constructor(private authService: AuthService, private router: Router) { }

	ngOnInit() {
	}

	register()
	{
		this.name = this.name.trim();
		this.password = this.password.trim();
		this.password_confirm = this.password_confirm.trim();
		this.email = this.email.trim();
		if(this.email === ''){
			this.email = this.name + '@pokemon.go';
		}
		let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(this.password !== '' && this.email !== '' && this.password_confirm !== '' && this.avatar !== '' && this.name !== ''){
			if(this.email.match(pattern)){
				if(this.password === this.password_confirm){
					let arr = {name: this.name, email: this.email, password: this.password, url_avatar: this.avatar}
					this.authService.create(arr).then(response => {
						if(response.id === undefined){
							this.resetFields();
							return;
						}
						this.router.navigateByUrl('home')
					});
				}
			}
		}
	}

	resetFields()
	{
		this.name = '';
		this.password = '';
		this.password_confirm = '';
		this.email = '';
		this.avatar = '';
	}

}
