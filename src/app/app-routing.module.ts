import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthService } from './services/auth.service';

const routes: Routes = [
	{ path: '', redirectTo: 'connection-screen', pathMatch: 'full' },
	{ path: 'home', canActivate: [AuthService], loadChildren: './home/home.module#HomePageModule' },
	{ path: 'connection-screen', loadChildren: './connection-screen/connection-screen.module#ConnectionScreenPageModule' },
	{ path: 'signin', loadChildren: './signin/signin.module#SigninPageModule' },
	{ path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
	{ path: 'app-login', loadChildren: './app-login/app-login.module#AppLoginPageModule' },
	{ path: 'app-signup', loadChildren: './app-signup/app-signup.module#AppSignupPageModule' },
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
	],
	exports: [RouterModule]
})
export class AppRoutingModule { }
