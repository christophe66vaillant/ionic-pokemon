import { Component } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { AuthService } from 'angularx-social-login';

import * as mapboxgl from 'mapbox-gl';

import { AuthService as AuthAppService } from '../services/auth.service';
import { User } from '../models/user';
import { PokemonService } from '../services/pokemon.service';
import { BasePokemon } from '../models/base-pokemon';
import { Pokemon } from '../models/pokemon';
import { Location } from '@angular/common';
import { GenerationService } from '../services/generation.service';
import { ToastController } from '@ionic/angular';

@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage{

	public isCatching: boolean = false;

	public toast: HTMLIonToastElement;

	public last_calcul: number = new Date().getTime();

	public coords_multiplier: number = 1000000; // 100 = 2 digits / 1000 = 3 digits.

	public distances: Object = {};

	public pokedex: Array<BasePokemon> = [];

	public pokemons: Array<Pokemon> = [];

	public player: mapboxgl.Marker;
	public pokemon: mapboxgl.Marker;

	public position: Array<mapboxgl.LngLatLike>;

	public map: mapboxgl.Map;
	public style: string = 'mapbox://styles/fakechris/cjtim9yj717gc1fk2jkopzr7a';

	public user: User = this.authAppService.user;

	public userLang: string = navigator.language.split('-', 1).shift();

	constructor(private toastController: ToastController, private generationService: GenerationService, private location: Location, private pokemonService: PokemonService, private androidPermissions: AndroidPermissions, private authService: AuthService, private authAppService: AuthAppService ) 
	{
		let el = document.createElement('div');
		el.className = 'marker';

		el.style.backgroundImage = `url('../assets/${this.user.url_avatar}')`;
		this.player = new mapboxgl.Marker(el);

		(mapboxgl as any).accessToken = 'pk.eyJ1IjoiZmFrZWNocmlzIiwiYSI6ImNqc3VjYjNvdTE3OHE0M3M5MDJmcThqem4ifQ.-Ec65JoEoJu8dr7NFDVtNg'
	}

	ionViewDidEnter()
	{
		this.addPokedexListener();
		if(this.location.isCurrentPathEqualTo('/home')){
			document.getElementsByClassName('cancel')[0].classList.add('hide');
		}

		if(localStorage.UUID == 'undefined'){
			this.signOut();
		}

		if(this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)){
			this.position = [];
			let coords: {lng: number, lat: number} = {lng: 0, lat: 0}
			navigator.geolocation.getCurrentPosition((response) => {
				coords.lng = response.coords.longitude;
				coords.lat = response.coords.latitude;

				this.map = new mapboxgl.Map({
					container: 'map',
					style: this.style,
					zoom: 19,
					center: coords,
					pitch: 60,
				});

				this.map.on('load', ()=>{
					document.getElementById('container').remove();
					document.getElementById('buttons').classList.remove('hide');
				})
				
				let actual_pos = {lng: Math.floor(coords.lng*this.coords_multiplier)/this.coords_multiplier, lat: coords.lat};
				this.user.position = actual_pos;
				this.authAppService.save(null, null, null, this.user.position);

				this.player.setLngLat([coords.lng, coords.lat]).addTo(this.map);

				this.position.push([coords.lng, coords.lat]);
		
				this.initPokemons();
			});
			
			navigator.geolocation.watchPosition( (response) => {

				coords.lng = response.coords.longitude;
				coords.lat = response.coords.latitude;

				this.addPokemonListener();

				let actual_pos = {lng: Math.floor(coords.lng*this.coords_multiplier)/this.coords_multiplier, lat: coords.lat};
				this.user.position = actual_pos;
				this.authAppService.save(null, null, null, this.user.position);

				if(!this.isCatching){
					this.player.setLngLat(coords);
					this.map.setCenter(this.player.getLngLat());
				}
			});
		}else{
			this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION);
		}
	}

	signOut()
	{
		if(this.authAppService.social_user !== null){
			this.authService.signOut();
		}else if(this.authAppService.user !== null){
			this.authAppService.user = null;
			localStorage.removeItem('UUID');
		}

		this.authAppService.goToPage('connection-screen');
	}

	initPokemons()
	{
		this.pokemonService.initPokemon().then(response => {
			for(let data of response){
				let pokemon = new BasePokemon(data);
				this.pokedex.push(pokemon);
			}
			this.pokemonService.pokedex = this.pokedex;
			this.spawnPokemons(100);
		});
	}

	spawnPokemons(count: number = 1)
	{
		for(let i=0;i<count;i++){
			let base_pokemon: BasePokemon = this.pokedex[Math.floor(Math.random() * this.pokedex.length)];
			
			let pokemon_attr = this.generationService.generateAttr(base_pokemon, this.player);
			
			let pokemon: Pokemon = new Pokemon({user_id: null, pokemon_id: base_pokemon.id, ATK: pokemon_attr.ATK, DEF: pokemon_attr.DEF, STA: pokemon_attr.STA, IV: pokemon_attr.IV, HP: pokemon_attr.HP, PC: pokemon_attr.PC, GENDER: pokemon_attr.GENDER, SHINY: pokemon_attr.SHINY, LEVEL: pokemon_attr.LEVEL} as Pokemon);
			this.pokemons.push(pokemon);

			let pulse = document.createElement('div');
			pulse.classList.add('pulse');

			let el = document.createElement('div');
			el.classList.add('marker');
			el.classList.add('pokemon');

			if(pokemon_attr.DISTANCE > 0.05){
				el.classList.add('hide');
				pulse.classList.add('stop')
			}
			el.style.backgroundPosition = `${base_pokemon.bgPosX}px ${base_pokemon.bgPosY}px`;
			pulse.append(el);
			this.distances[pokemon_attr.DISTANCE] = pulse;
			this.pokemon = new mapboxgl.Marker(pulse);
			this.pokemon.setLngLat(pokemon_attr.SPAWN);
			this.pokemon.addTo(this.map);

			this.pokemons.sort((a, b) => {
				return a.pokemon_id - b.pokemon_id;
			});
			this.addListener(el, pulse, pokemon_attr, base_pokemon);
		}
	}

	private addListener(el: HTMLDivElement, pulse: HTMLDivElement, attr: any, basePokemon: BasePokemon) {

		this.addButtonsListener(el, pulse, attr);
		let container = document.getElementById('canvas-container') as HTMLDivElement;
		let canvas = document.getElementById('canvas') as HTMLCanvasElement;
		let ctx = canvas.getContext('2d');

		canvas.style.width='100%';
		canvas.style.height='100%';
		canvas.height = container.offsetHeight;
		canvas.width = container.offsetWidth;

		pulse.addEventListener('click', () => {

			ctx.font = "15px Arial";
			ctx.textAlign="center";
			ctx.strokeText('test', 150, 120);

			let arr = this.user.items.find((arr_item)=>{
				if(arr_item.quantity !== 0){
					arr_item.quantity -= 1;
					attr['quantity'] = arr_item.quantity;
					attr['item_id'] = arr_item.item_id;
					return true;
				}else{
					return false;
				}
			});

			if(!arr){
				this.pokeballToast();
				return;
			}

			if(attr.SHINY){
				el.classList.add('shiny');
			}

			el.classList.add('selected');

			let pokemonName = document.createElement('div');
			if(this.userLang == 'fr'){
				pokemonName.textContent = basePokemon.fr_name;	
			}else{
				pokemonName.textContent = basePokemon.name;
			}

			el.append(pokemonName);

			this.isCatching = true;

			pulse.classList.add('stop')
			container.classList.remove('hide');
			canvas.classList.remove('hide');
			canvas.classList.add('battle');

			this.map.flyTo({center: attr.SPAWN});

			for(let [key, value] of Object.entries(this.distances)){
				if(pulse !== value){
					value.style.display = 'none';
					this.player.remove()
				}
			}

			pulse.classList.add('pokemon_battle');

			pulse.addEventListener('click', () => {
				this.isCatching = false;
				this.player.addTo(this.map);
				this.map.flyTo({center: this.player.getLngLat()})
				container.classList.add('hide')
				canvas.classList.remove('battle');
				canvas.classList.add('hide');
				for(let [key, value] of Object.entries(this.distances)){
					value.style.display = '';
				}

				attr['user_id'] = this.user.id;
				this.authAppService.save(attr);
				pulse.remove();
				this.pokemons.find((pokemon, index) => {
					if(pokemon.id === basePokemon.id){
						this.pokemons.splice(index, 1)
						return true;
					}
					return false;
				})
				this.spawnPokemons();
			})
		});
	}

	private addPokemonListener()
	{
		setTimeout(() => {
			for(let [key, value] of Object.entries(this.distances)){
				if(parseInt(key) < 0.1){
					value.classList.remove('hide');
				}else{
					value.classList.add('hide');
				}
			}
		}, 1000)
	}

	async pokeballToast() {
		if(this.toast){
			this.toast.dismiss();
		}
		let text: string = '';
		if(this.userLang == 'fr'){
			text = `Vous n'avez pas assez de Pokéballs pour attraper ce Pokémon.`;
		} else text = 'You don\'t have enough Balls to catch Pokémons.';
		this.toast = await this.toastController.create({
			message: text,
			duration: 3000,
			buttons: [
				{
					icon: 'close',
					role: 'cancel',
				}
			  ]
		});
		this.toast.present();
	}

	private addButtonsListener(el: HTMLDivElement, pulse: HTMLDivElement, attr: any)
	{
		if(this.isCatching == true) return;
		document.getElementById('home').addEventListener('click', () => {
			pulse.classList.add('stop');
			el.classList.add('hide');
			this.player.remove();
			document.getElementById('home').style.opacity = '0';
			document.getElementById('home').style.zIndex = '8';
			document.getElementById('close').style.opacity = '1';
			document.getElementById('close').style.transform = 'scale(1,1)'
			document.getElementById('button-background').style.opacity = '1';
			document.getElementById('button-background').classList.remove('hide');
			document.getElementById('b1').style.opacity = '1'
			document.getElementById('b1').style.transform = 'translate(-80px, -30px)';
			document.getElementById('b2').style.opacity = '1'
			document.getElementById('b2').style.transform = 'translate(80px,-30px)'
			document.getElementById('b3').style.opacity = '1'
			document.getElementById('b3').style.transform = 'translate(0px,-100px)'
		});

		document.getElementById('close').addEventListener('click', () => {
			if(attr.DISTANCE < 0.05){
				pulse.classList.remove('stop');
				el.classList.remove('hide');
			}
			if(!this.isCatching){
				this.player.addTo(this.map);
			}
			document.getElementById('close').style.opacity = '0'
			document.getElementById('close').style.transform = 'scale(0, 0)'
			document.getElementById('home').style.opacity = '1'
			document.getElementById('home').style.zIndex = '10'
			document.getElementById('button-background').style.opacity = '0';
			document.getElementById('button-background').classList.add('hide');
			document.querySelectorAll('#b1, #b2, #b3, #b4').forEach((item) => {
				item.setAttribute('style', 'opacity:0')
				item.setAttribute('style', 'transform:translate(0px,0px)')
			})

			setTimeout(function(){
			  document.getElementById('close').style.transform = 'scale(1.5,1.5)';
			}, 100);
		});
	}

	addPokedexListener()
	{
		document.getElementById('b3').addEventListener('click', () => {
			document.getElementById('app-pokedex').classList.add('open');
			document.getElementById('app-pokedex').classList.remove('hide');
			document.getElementById('buttons').classList.add('hide');
			document.getElementsByClassName('cancel')[0].classList.remove('hide');
			document.getElementsByClassName('cancel')[0].addEventListener('click', () => {
				document.getElementById('app-pokedex').classList.add('hide');
				document.getElementById('app-pokedex').classList.remove('open');
				document.getElementById('buttons').classList.remove('hide');
				document.getElementsByClassName('cancel')[0].classList.add('hide');
			})
		})
	}
}
