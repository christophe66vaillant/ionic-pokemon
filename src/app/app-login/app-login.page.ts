import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
	selector: 'app-app-login',
	templateUrl: './app-login.page.html',
	styleUrls: ['./app-login.page.scss'],
})
export class AppLoginPage implements OnInit {

	public email: string = '';
	public password: string = '';

	constructor(private router: Router, private authService: AuthService) { }

	ngOnInit() {
	}

	login()
	{
		this.email = this.email.trim();
		this.password = this.password.trim();

		let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		if(this.email !== '' && this.password !== ''){
			if(this.email.match(pattern)){
				let arr = {email: this.email, password: this.password};
				this.authService.login(arr).then(response => {
					this.resetFields();
					if((response.id === undefined) || response.banned === true){
					}else{
						this.router.navigateByUrl('home');
					}
				});
			}
		}
	}

	resetFields()
	{
		this.email = '';
		this.password = '';
	}

}
