import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
	selector: 'app-connection-screen',
	templateUrl: './connection-screen.page.html',
	styleUrls: ['./connection-screen.page.scss'],
})
export class ConnectionScreenPage implements OnInit {

	constructor(private authService: AuthService) { }

	ngOnInit()
	{
		if(localStorage.UUID){
			this.authService.autoConnect().then(response => this.goToPage('home') );
		}
	}

	goToPage(pageName: string)
	{
		this.authService.goToPage(pageName);
	}

}
