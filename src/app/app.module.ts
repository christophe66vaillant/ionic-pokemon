import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BackbuttonComponent } from './backbutton/backbutton.component';

let config = new AuthServiceConfig([
	{
		id: GoogleLoginProvider.PROVIDER_ID,
		provider: new GoogleLoginProvider("128306763085-itm6s48mlaqsgd44c82gs0v0nk9rurrt.apps.googleusercontent.com")
	},
	{
		id: FacebookLoginProvider.PROVIDER_ID,
		provider: new FacebookLoginProvider("348281725891660")
	}
]);
 
export function provideConfig() {
  return config;
}

@NgModule({
	declarations: [AppComponent, BackbuttonComponent],
	entryComponents: [],
	imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, SocialLoginModule],
	providers: [
		AndroidPermissions,
		Geolocation,
		StatusBar,
		SplashScreen,
		{ provide: AuthServiceConfig, useFactory: provideConfig },
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
	],
	exports: [BackbuttonComponent],
	bootstrap: [AppComponent]
})
export class AppModule {}
