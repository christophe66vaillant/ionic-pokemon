import { ModelLiteral, Model } from './model';

export class Egg extends Model
{
	public label: string;
	public fr_label: string;
	public bgPosX: number;

	constructor(eggLiteral: EggLiteral)
	{
		super(eggLiteral);

		this.label = eggLiteral.label;
		this.fr_label = eggLiteral.fr_label;
		this.bgPosX = eggLiteral.bgPosX;
	}
}

export interface EggLiteral extends ModelLiteral
{
	label: string;
	fr_label: string;
	bgPosX: number;
}