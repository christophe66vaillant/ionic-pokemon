import { Model, ModelLiteral } from './model';

export class Candy extends Model
{
	public user_id: number;
	public candy_id: number;
	public quantity: number;

	constructor(candyLiteral: CandyLiteral)
	{
		super(candyLiteral);
		
		this.user_id = candyLiteral.user_id;
		this.candy_id = candyLiteral.candy_id;
		this.quantity = candyLiteral.quantity;
	}
}

export interface CandyLiteral extends ModelLiteral
{
	user_id: number;
	candy_id: number;
	quantity: number;
}
