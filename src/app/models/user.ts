import { Model, ModelLiteral } from './model';
import { Pokemon, PokemonLiteral } from './pokemon';
import { ItemUser, ItemUserLiteral } from './item-user';
import { EggUser, EggUserLiteral } from './egg-user';

export class User extends Model
{
	public name: string;
	public email: string;
	public level: number;
	public xp: number;
	public role_id: number;
	public kms: number;
	public banned: boolean;
	public url_avatar: string;
	private readonly uuid: string;
	public pokemons: Array<Pokemon> = [];
	public items: Array<ItemUser> = [];
	public eggs: Array<EggUser> = [];
	public position: {lng: number, lat: number};

	constructor(userLiteral: UserLiteral)
	{
		super(userLiteral);
		this.name = userLiteral.name;
		this.email = userLiteral.email;
		this.level = userLiteral.level;
		this.xp = userLiteral.xp;
		this.role_id = userLiteral.role_id;
		this.kms = userLiteral.kms;
		this.banned = userLiteral.banned;
		this.url_avatar = userLiteral.url_avatar;
		if(localStorage.UUID){
			this.uuid = localStorage.UUID;
		}else{
			this.uuid = userLiteral.uuid;
		}
		localStorage.setItem('UUID', this.uuid);

		if(userLiteral.pokemons){
			for(let pokemon of userLiteral.pokemons){
				let pokemon_send = new Pokemon(pokemon);
				this.pokemons.push(pokemon_send);
			}
		}

		if(userLiteral.items){
			for(let item of userLiteral.items){
				if(item.quantity !== 0){
					let item_send = new ItemUser(item);
					this.items.push(item_send);
				}
			}
		}

		if(userLiteral.eggs){
			for(let egg of userLiteral.eggs){
				let new_egg = new EggUser(egg);
				this.eggs.push(new_egg);
			}
		}

		if(userLiteral.position){
			this.position = {lng: userLiteral.position.last_lng, lat: userLiteral.position.last_lat};
		}
	}

	getUuid()
	{
		return this.uuid;
	}

	setPosition(position: {lng: number, lat: number})
	{
		return this.position = position;
	}
}

export interface UserLiteral extends ModelLiteral
{
	name: string;
	email: string;
	level: number;
	xp: number;
	role_id: number;
	kms: number;
	banned: boolean;
	url_avatar: string;
	uuid: string;
	pokemons?: Array<PokemonLiteral>;
	items?: Array<ItemUserLiteral>;
	eggs?: Array<EggUserLiteral>;
	position?: {last_lng: number, last_lat:number}
}
