export class Model 
{
	public id:number;

	constructor(modelLiteral: ModelLiteral)
	{
		this.id = modelLiteral.id;
	}
}

export interface ModelLiteral
{
	id:number;
}