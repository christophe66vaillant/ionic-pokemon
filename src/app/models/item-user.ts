import { Model, ModelLiteral } from './model';

export class ItemUser extends Model
{
    public user_id: number;
    public item_id: number;
    public quantity: number;

    constructor(itemUserLiteral: ItemUserLiteral)
    {
        super(itemUserLiteral);
        
        this.user_id = itemUserLiteral.user_id;
        this.item_id = itemUserLiteral.item_id;
        this.quantity = itemUserLiteral.quantity;
    }
}

export interface ItemUserLiteral extends ModelLiteral
{
    user_id: number;
    item_id: number;
    quantity: number;
}
