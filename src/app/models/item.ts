import { ModelLiteral, Model } from './model';

export class Item extends Model
{
	public label: string;
	public fr_label: string;
	public description: string;
	public fr_description: string;
	public bgPosX: number;

	constructor(itemLiteral: ItemLiteral)
	{
		super(itemLiteral);

		this.label = itemLiteral.label;
		this.fr_label = itemLiteral.fr_label;
		this.description = itemLiteral.description;
		this.fr_description = itemLiteral.fr_description;
		this.bgPosX = itemLiteral.bgPosX;
	}
}

export interface ItemLiteral extends ModelLiteral
{
	label: string;
	fr_label: string;
	description: string;
	fr_description: string;
	bgPosX: number;
}