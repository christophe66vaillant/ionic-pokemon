import { Model, ModelLiteral } from './model';

export class Pokemon extends Model
{
	public user_id: number;
	public pokemon_id: number;
	public ATK: number;
	public DEF: number;
	public STA: number;
	public IV: number;
	public HP: number;
	public PC: number;
	public GENDER: number|boolean;
	public SHINY: number|boolean;
	public LEVEL: number;

	constructor(pokemonLiteral: PokemonLiteral)
	{	
		super(pokemonLiteral);
			
		this.user_id = pokemonLiteral.user_id;
		this.pokemon_id = pokemonLiteral.pokemon_id;
		this.ATK = pokemonLiteral.ATK;
		this.DEF = pokemonLiteral.DEF;
		this.STA = pokemonLiteral.STA;
		this.IV = pokemonLiteral.IV;
		this.HP = pokemonLiteral.HP;
		this.PC = pokemonLiteral.PC;
		this.GENDER = pokemonLiteral.GENDER;
		this.SHINY = pokemonLiteral.SHINY;
		this.LEVEL = pokemonLiteral.LEVEL;
	}
}

export interface PokemonLiteral extends ModelLiteral
{
	user_id: number;
	pokemon_id: number;
	ATK: number;
	DEF: number;
	STA: number;
	IV: number;
	HP: number;
	PC: number;
	GENDER: number|boolean;
	SHINY: number|boolean;
	LEVEL: number;
}
