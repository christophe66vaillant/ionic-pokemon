import { Model, ModelLiteral } from './model';

export class BasePokemon extends Model
{
	public name: string;
	public fr_name: string;
	public bgPosX: number;
	public bgPosY: number;
	public next_evolution: number;
	public base_hp: number;
	public base_attack: number;
	public base_defense: number;
	public next_evolution_cost: number;
	public level_id: number;
	public candyPosX: number;
	public candyPosY: number;

	constructor(basePokemonLiteral: BasePokemonLiteral)
	{
		super(basePokemonLiteral);

		this.name = basePokemonLiteral.name;
		this.fr_name = basePokemonLiteral.fr_name;
		this.bgPosX = basePokemonLiteral.bgPosX;
		this.bgPosY = basePokemonLiteral.bgPosY;
		this.next_evolution = basePokemonLiteral.next_evolution;
		this.base_hp = basePokemonLiteral.base_hp;
		this.base_attack = basePokemonLiteral.base_attack;
		this.base_defense = basePokemonLiteral.base_defense;
		this.next_evolution_cost = basePokemonLiteral.next_evolution_cost;
		this.level_id = basePokemonLiteral.level_id;
		this.candyPosX = basePokemonLiteral.candyPosX;
		this.candyPosY = basePokemonLiteral.candyPosY;
	}
}

export interface BasePokemonLiteral extends ModelLiteral
{
	name: string;
	fr_name: string;
	bgPosX: number;
	bgPosY: number;
	next_evolution: number;
	base_hp: number;
	base_attack: number;
	base_defense: number;
	next_evolution_cost: number;
	level_id: number;
	candyPosX: number;
	candyPosY: number;
}
