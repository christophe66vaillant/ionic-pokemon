import { Model, ModelLiteral } from './model';

export class Event extends Model
{
	public label: string;
	public fr_label: string;
	public description: string;
	public fr_description: string;
	public type_id: number;
	public xp_rate: number;
	public start_time: Date;
	public end_time: Date;

	constructor(eventLiteral: EventLiteral)
	{
		super(eventLiteral);
		
		this.label = eventLiteral.label;
		this.fr_label = eventLiteral.fr_label;
		this.description = eventLiteral.description;
		this.fr_description = eventLiteral.fr_description;
		this.type_id = eventLiteral.type_id;
		this.xp_rate = eventLiteral.xp_rate;
		this.start_time = eventLiteral.start_time;
		this.end_time = eventLiteral.end_time;
	}
}

export interface EventLiteral extends ModelLiteral
{
	label: string;
	fr_label: string;
	description: string;
	fr_description: string;
	type_id: number;
	xp_rate: number;
	start_time: Date;
	end_time: Date;
}