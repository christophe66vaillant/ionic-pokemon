import { Model, ModelLiteral } from './model';

export class EggUser extends Model
{
    public user_id: number;
    public egg_id: number;
    public kms: number;
    public date: Date;

    constructor(eggUserLiteral: EggUserLiteral)
    {
        super(eggUserLiteral);
        this.user_id = eggUserLiteral.user_id;
        this.egg_id = eggUserLiteral.egg_id;
        this.kms = eggUserLiteral.kms;
        this.date = eggUserLiteral.date;
    }
}

export interface EggUserLiteral extends ModelLiteral
{
    user_id: number;
    egg_id: number;
    kms: number;
    date: Date;
}
