import { Point } from "./Point";

export class Tools
{
    public static levels: Array<number> = [
        0.094,
        0.16639787,
        0.21573247,
        0.25572005,
        0.29024988,
        0.3210876,
        0.34921268,
        0.3752356,
        0.39956728,
        0.4225,
        0.44310755,
        0.4627984,
        0.48168495,
        0.49985844,
        0.51739395,
        0.5343543,
        0.5507927,
        0.5667545,
        0.5822789,
        0.5974,
        0.6121573,
        0.6265671,
        0.64065295,
        0.65443563,
        0.667934,
        0.6811649,
        0.69414365,
        0.7068842,
        0.7193991,
        0.7317,
        0.73776948,
        0.74378943,
        0.74976104,
        0.75568551,
        0.76156384,
        0.76739717,
        0.7731865,
        0.77893275,
        0.78463697,
        0.79030001
    ];

    constructor(){}

    static getRandomInt(min: number, max:number)
    {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    static getRandomFloat(min: number, max:number)
    {
        return Math.random() * (max - min +1) + min;
    }

    static getRandomLngLat(min: number, max:number)
    {
        return Math.random() * (max - min) + min;
    }

    static degreesToRadians(degrees:number) {
        return degrees * Math.PI / 180;
    }
      
    static distanceInKmBetweenEarthCoordinates(position1:Point, position2:Point) {
        var earthRadiusKm = 6371;
        
        var dLat = this.degreesToRadians(position2.lat-position1.lat);
        var dLng = this.degreesToRadians(position2.lng-position1.lng);
        
        var lat1 = this.degreesToRadians(position1.lat);
        var lat2 = this.degreesToRadians(position2.lat);
        
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLng/2) * Math.sin(dLng/2) * Math.cos(lat1) * Math.cos(lat2); 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        return earthRadiusKm * c;
    }
}
