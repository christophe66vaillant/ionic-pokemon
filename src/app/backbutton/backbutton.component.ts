import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
	selector: 'app-backbutton',
	templateUrl: './backbutton.component.html',
	styleUrls: ['./backbutton.component.scss'],
})
export class BackbuttonComponent implements OnInit {

	constructor(private location: Location) { }

	ngOnInit() {}

	goBack()
	{
		if(this.location.isCurrentPathEqualTo('/home')){
			const cancel_btn = document.getElementsByClassName('cancel');
			return;
		}
		return this.location.back();
	}

}
