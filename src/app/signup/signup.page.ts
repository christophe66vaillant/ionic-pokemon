import { Component, OnInit } from '@angular/core';
import { AuthService as AuthAppService } from '../services/auth.service';
import { AuthService, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';

@Component({
	selector: 'app-signup',
	templateUrl: './signup.page.html',
	styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

	private loggedIn: boolean;

	constructor(private authService: AuthService, private authAppService: AuthAppService) { }
	
	signInWithGoogle(): void {
		this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
	}
	
	signInWithFB(): void {
		this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
	}
	
	signOut(): void {
		this.authService.signOut();
	}
	
	ngOnInit() {
		this.authService.authState.subscribe((user) => {
			this.authAppService.social_user = user;
			this.loggedIn = (user != null);
		});
	}

	goToPage(pageName: string)
	{
		this.authAppService.goToPage(pageName);
	}
}
