import { Injectable } from '@angular/core';
import { BasePokemon } from '../models/base-pokemon';

@Injectable({
  	providedIn: 'root'
})
export class PokemonService {

	public pokedex: Array<BasePokemon> = [];

	private readonly url: string = 'http://127.0.0.1:8000/api/pokedex';

	constructor() { }
	
	async initPokemon()
	{
		const response = await fetch(`${this.url}`);
		const datajson = await response.json();
		
		return datajson.pokemons;
	}
}
