import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { SocialUser } from 'angularx-social-login';
import { Pokemon } from '../models/pokemon';
import { ItemUser } from '../models/item-user';
import { EggUser } from '../models/egg-user';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit {

	public user: User = null;
	public social_user: SocialUser = null;
	private url: string = 'http://127.0.0.1:8000/api/player';

	constructor(private router: Router) { }
	
	ngOnInit(){}

	goToPage(pageName: string)
	{
		this.router.navigate([pageName]);
	}

	async autoConnect()
	{
		let form: FormData = new FormData;
		form.append('uuid', localStorage.UUID);

		const response = await fetch(`${this.url}/auth`, {body: form, method: 'POST'});
		const datajson = await response.json();
		if(!datajson.user){
			localStorage.removeItem('UUID');
			return;
		}
		this.user = new User(datajson.user);
		return this.user;
	}

	async create(arr: {name: string, password: string, email: string, url_avatar: string})
	{
		let uuid: string = this.generator();

		arr['uuid'] = uuid;

		let data: FormData = new FormData;
		for(let [key, value] of Object.entries(arr)){
			data.append(key, value);
		}

		const response = await fetch(this.url, {body: data, method: 'POST'});
		const datajson = await response.json();

		if(datajson.user){
			localStorage.setItem('UUID', uuid);
			this.user = new User(datajson.user);
			let item = new ItemUser({item_id: 1, user_id: datajson.user.id, quantity: 30} as ItemUser);
			this.save(null, item);
			return this.user;
		}
		return datajson;
	}

	async login(arr: {email: string, password: string})
	{
		let form: FormData = new FormData;
		for(let [key, value] of Object.entries(arr)){
			form.append(key, value);
		}

		const response = await fetch(`${this.url}/auth`, {body: form, method: 'POST'});
		const datajson = await response.json();
		this.user = new User(datajson.user);
		return this.user;
	}

	async save(pokemon: Pokemon = null, item: ItemUser = null, egg: EggUser = null, position: mapboxgl.LngLatLike = null, kms: number = null)
	{
		let url = `${this.url}/${this.user.id}/save`;
		let data = new FormData();
		if(this.user.id !== (null || undefined)){
			data.append('user_id', this.user.id.toString());
		}
		if(pokemon){
			for(let [key, value] of Object.entries(pokemon)){
				data.append(key, value);
			}
		}

		if(item){
			for(let [key, value] of Object.entries(item)){
				data.append(key, value);
			}
		}

		if(egg){
			for(let [key, value] of Object.entries(egg)){
				data.append(key, value);
			}
		}

		if(position){
			for(let [key, value] of Object.entries(position)){
				data.append(key, value);
			}
		}

		if(kms){
			for(let [key, value] of Object.entries(kms)){
				data.append(key, value);
			}
		}
		const response = await fetch(url, {body: data, method: 'POST'});
		const datajson = await response.json();
		if(position){
			this.user.position = {lng: datajson.user.last_lng, lat: datajson.user.last_lat};
		}
		if(egg){
			this.user.eggs.find((arr_egg, index)=>{
				if(arr_egg.id === egg.id){
					this.user.eggs.splice(index, 1);
					return true;
				}
			})
			this.user.eggs.push(new EggUser(datajson.egg));
		}
		if(item){
			this.user.items.find((arr_item, index)=>{
				if(arr_item.id === item.id){
					this.user.items.splice(index, 1);
					return true;
				}
			})
			this.user.items.push(new ItemUser(datajson.item));
		}
		if(pokemon){
			this.user.pokemons.find((arr_pokemon, index)=>{
				if(arr_pokemon.id === pokemon.id){
					this.user.pokemons.splice(index, 1);
					return true;
				}
			})
			this.user.pokemons.push(new Pokemon(datajson.pokemon));
		}

		if(kms){
			this.user.kms = kms;
		}
		
		return this.user;
	}

	async getPokemons()
	{
		this.user.pokemons = [];
		const response = await fetch(`${this.url}/${this.user.id}`);
		const datajson = await response.json();
		
		return datajson.user.pokemons;
	}

	canActivate()
	{
		if((this.user || this.social_user) !== null){
			return true;
		}

		this.goToPage('connection-screen')
		return false;
	}

	private generator(): string {
		const isString = `${this.S4()}${this.S4()}-${this.S4()}-${this.S4()}-${this.S4()}-${this.S4()}${this.S4()}${this.S4()}`;

		return isString;
	}
		
	private S4(): string {
		return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
	}
}
