import { Injectable } from '@angular/core';
import { Tools } from '../models/tools';
import { Point } from '../models/point';
import { BasePokemon } from '../models/base-pokemon';

@Injectable({
    providedIn: 'root'
})
export class GenerationService {

    constructor() { }

    generateAttr(pokemon: BasePokemon, player: mapboxgl.Marker)
    {
      // GENDER
      let genders: Array<string> = ['male', 'female'];
      let gender: boolean|string = genders[Math.floor(Math.random() * genders.length)];

      gender = gender == 'male' ? true : false;

      // SHINY
      let shiny: boolean|number = Tools.getRandomInt(10, 10000);

      shiny = shiny < 250000;

      // IV

      let atk: number = Tools.getRandomInt(0, 15);
      let def: number = Tools.getRandomInt(0, 15);
      let sta: number = Tools.getRandomInt(0, 15);
      let iv: number = Math.round((atk + def + sta) / 45 * 100);

      // CP AND HP AND POKEMON'S LEVEL

      let position = player.getLngLat().toArray();

      const pokemon_spawn = new Point(Tools.getRandomLngLat(position[0] + 0.00218048625301, position[0] - 0.00260645321505), Tools.getRandomLngLat(position[1] + 0.002150559040753, position[1] - 0.002146797979975));

      let distance = Tools.distanceInKmBetweenEarthCoordinates(new Point(position[0], position[1]), pokemon_spawn);

      let LEVEL: number = Math.floor(Math.random() * (40 - 1) + 1);
      let multiple: number = Tools.levels[LEVEL - 1];
      let pc: number = Math.trunc(( (pokemon.base_attack+atk) * Math.pow( (pokemon.base_defense+def), 0.5 ) * Math.pow( (pokemon.base_hp+sta), 0.5 ) ) * (Math.pow(multiple, 2) / 10));
      let final_hp: number = Math.floor((pokemon.base_hp + sta) * multiple);

      return {'pokemon_id': pokemon.id, 'SPAWN': pokemon_spawn, 'DISTANCE': distance, 'ATK': atk, 'STA': sta,'DEF': def,'LEVEL': LEVEL,'PC': pc,'HP': final_hp,'GENDER': gender,'IV': iv,'SHINY': shiny};
    }
}
