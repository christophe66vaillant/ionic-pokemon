import { Component, OnInit } from '@angular/core';

import { AuthService } from "angularx-social-login";
import { AuthService as AuthAppService } from "../services/auth.service";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";

@Component({
	selector: 'app-signin',
	templateUrl: './signin.page.html',
	styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {

	private loggedIn: boolean;

	constructor(private authService: AuthService, private authAppService: AuthAppService) { }
	
	signInWithGoogle(): void {
		this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
	}
	
	signInWithFB(): void {
		this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
	}
	
	ngOnInit() {
		this.authService.authState.subscribe((user) => {
			this.authAppService.social_user = user;
			this.loggedIn = (user != null);
		});
	}

	goToPage(pageName: string)
	{
		this.authAppService.goToPage(pageName);
	}
}
