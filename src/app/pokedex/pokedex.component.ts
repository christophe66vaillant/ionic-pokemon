import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../services/pokemon.service';
import { BasePokemon } from '../models/base-pokemon';
import { AuthService } from '../services/auth.service';
import { Pokemon } from '../models/pokemon';

@Component({
	selector: 'app-pokedex',
	templateUrl: './pokedex.component.html',
	styleUrls: ['./pokedex.component.scss'],
})
export class PokedexComponent implements OnInit {

	public pokedex: Array<BasePokemon> = [];
	public pokemons: Array<Pokemon> = [];
	public elements: Array<HTMLDivElement> = [];

	public container: HTMLElement;
	public title: HTMLElement;
	public divider: HTMLElement;
	public padding: HTMLElement;

	constructor(private authService: AuthService, private pokemonService: PokemonService) {}
	
	async ngOnInit()
	{
		this.container = document.getElementById('pokedex-container');
		let b3 = await document.getElementById('b3');
		b3.addEventListener('click', ()=>{
			this.checkPokedex();

			document.getElementById('cancel').addEventListener('click', ()=>{
				this.removePokedex();
			})
		})
	}

	removePokedex()
	{
		let pokedex = document.getElementsByClassName('pokemon-on-pokedex');
		for(let [key, value] of Object.entries(pokedex)){
			value.remove();
		}
		this.pokedex = [];
		this.elements = [];
		this.pokemons = [];
	}

	checkPokedex()
	{
		this.authService.getPokemons().then((response) => {
			for(let data of response){
				let pokemon = new Pokemon(data);
				this.pokemons.push(pokemon);
			}
		})
		this.pokemonService.initPokemon().then((response) => {
			let insert_before = document.getElementById('insert-before');

			for(let data of response){
				let base_pokemon = new BasePokemon(data);
				this.pokedex.push(base_pokemon);

			}
			for(let base_pokemon of this.pokedex){
				let el = document.createElement('div');
				el.classList.add('item', 'pokemon', 'unknown', 'pokemon-on-pokedex');
				el.style.backgroundPosition = `${base_pokemon.bgPosX}px ${base_pokemon.bgPosY}px`;
				let t = this.container.getAttributeNames();
				el.setAttribute(t[0], '')
				for(let pokemon of this.pokemons){
					if(base_pokemon.id === pokemon.pokemon_id){
						el.classList.remove('unknown');
					}
				}
				this.elements.push(el);
				this.container.insertBefore(el, insert_before);
			}
		});

	}
}
